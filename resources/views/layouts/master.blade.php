<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
    <title>Coffee Break a Blog Category Flat Bootstarp responsive Website Template | Home :: w3layouts</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="keywords" content="Coffee Break Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design"/>
    <script type="application/x-javascript"> addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);
        function hideURLbar() {
            window.scrollTo(0, 1);
        } </script>
    <link href="{!! asset('css/bootstrap.css') !!}" rel='stylesheet' type='text/css'/>
    <link href="{!! asset('css/style.css') !!}" rel='stylesheet' type='text/css'/>
    <script src="{!! asset('js/jquery.min.js') !!}"></script>
    <!---- start-smoth-scrolling---->
    <script type="text/javascript" src="{!! asset('js/move-top.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/easing.js') !!}"></script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $(".scroll").click(function (event) {
                event.preventDefault();
                $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1000);
            });
        });
    </script>
    <!--start-smoth-scrolling-->
</head>
<body>
<!--header-top-starts-->
@include('partials.header-top')
<!--header-top-end-->
<!--start-header-->
@include('partials.menu')
<!-- script-for-menu -->
<!--banner-starts-->
@include('partials.banner')
<!--banner-end-->

@yield('content')
        <!--slide-starts-->
@include('partials.slide',compact('images'))
<!--slide-end-->
<!--footer-starts-->
@include('partials.footer')
<!--footer-end-->
</body>
</html>