<div class="header">
    <div class="container">
        <div class="head">
            <div class="navigation">
                <span class="menu"></span>
                <ul class="navig">
                    <li><a href="/ams/public/" class="active">Home</a></li>
                    <li><a href="/ams/public/about">About</a></li>
                    <li><a href="/ams/public/gallery">Gallery</a></li>
                    <li><a href="/ams/public/contact">Contact</a></li>
                    @if (Auth::guest())
                    <li><a href="/ams/public/login">Login</a></li>
                    <li><a href="/ams/public/register">Register</a></li>
                    @else
                        <li><a href="/ams/public/logout">Logout</a></li>
                        <li><a href="/ams/public/admin">Go For Post</a></li>
                    @endif
                </ul>
            </div>
            <div class="header-right">
                <div class="search-bar">
                    <input type="text" value="Search" onfocus="this.value = '';"
                           onblur="if (this.value == '') {this.value = 'Search';}">
                    <input type="submit" value="">
                </div>
                <ul>
                    <li><a href="#"><span class="fb"> </span></a></li>
                    <li><a href="#"><span class="twit"> </span></a></li>
                    <li><a href="#"><span class="pin"> </span></a></li>
                    <li><a href="#"><span class="rss"> </span></a></li>
                    <li><a href="#"><span class="drbl"> </span></a></li>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- script-for-menu -->
<!-- script-for-menu -->
<script>
    $("span.menu").click(function () {
        $(" ul.navig").slideToggle("slow", function () {
        });
    });
</script>