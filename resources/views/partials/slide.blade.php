{{--{{ dd($images) }}--}}
<div class="slide">
    <div class="container">
        <div class="fle-xsel">
            <ul id="flexiselDemo3">
                @foreach($images as $image)
                <li>
                    <a href="#">
                        <div class="banner-1">
{{--                            @foreach($oneImages as $image)--}}
                            <img src="{{ $image->img_path }}" class="" height="120px" width="200px" alt="">
                                {{--@endforeach--}}
                        </div>
                    </a>
                </li>

                    @endforeach

            </ul>

            <script type="text/javascript">
                $(window).load(function () {

                    $("#flexiselDemo3").flexisel({
                        visibleItems: 5,
                        animationSpeed: 1000,
                        autoPlay: true,
                        autoPlaySpeed: 3000,
                        pauseOnHover: true,
                        enableResponsiveBreakpoints: true,
                        responsiveBreakpoints: {
                            portrait: {
                                changePoint: 480,
                                visibleItems: 2
                            },
                            landscape: {
                                changePoint: 640,
                                visibleItems: 3
                            },
                            tablet: {
                                changePoint: 768,
                                visibleItems: 3
                            }
                        }
                    });

                });
            </script>
            <script type="text/javascript" src="js/jquery.flexisel.js"></script>
            <div class="clearfix"></div>
        </div>
    </div>
</div>