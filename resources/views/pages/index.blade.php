@extends('layouts.master')

@section('content')

        <!--about-starts-->
<div class="about">
    <div class="container">
        <div class="about-main">
            <div class="col-md-8 about-left">

                <div class="about-tre">
                    <div class="a-1">
                        @foreach($indexArticles as $article)
                        <div class="col-md-6 abt-left" >
                            @foreach($article->images as $images)
                            <a href="{{url('admin/article/'.$article->id)}}"><img src="{!! asset($images->img_path) !!}" height="200px"  alt=""/></a>
                            @endforeach
                            <h3><a href="{{url('admin/article/'.$article->id)}}">{!! $article->title !!}</a></h3>

                            <p>{!! $article->html_summary !!}</p>
                           <div class="a-btn">
                            <a href="{{url('admin/article/'.$article->id)}}">Read More</a>
                               </div>
                        </div>
                        @endforeach



                        <div class="clearfix"></div>
                    </div><br><br>
                    <div class="text-center">
                        {!! $indexArticles->links() !!}
                    </div>
                </div>
            </div>
            <div class="col-md-4 about-right heading">

                <div class="abt-2">
                    <h3>RECENT POSTS</h3>
                    @foreach($sidebarArticles as $article)
                    <div class="might-grid">
                        <div class="grid-might">
                            @foreach($article->images as $images)
                            <a href="{{url('admin/article/'.$article->id)}}"><img src="{!! asset($images->img_path) !!}" class="" height="100px" width="100px" alt=""> </a>
                        @endforeach
                        </div>
                        <div class="might-top">
                            <h4><a href="{{url('admin/article/'.$article->id)}}">{{$article->title}}</a></h4>

                            <p>{!! $article->html_summary !!}</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                        @endforeach
                </div>
                <div class="abt-2">
                    <h3>CATEGORIES</h3>
                    <ul>
                        @foreach($categories as $category)
                        <li><a href="category/{{ $category->id }}">{!! $category->title !!} </a></li>
                        @endforeach
                    </ul>
                </div>

            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!--about-end-->
@endsection