@extends('layouts.master')

@section('content')

        <!--start-single-->
<div class="single">
    <div class="container">
        <div class="single-top">
            @foreach($article->images as $images)
            <a href="#"><img class="" src="{!! asset($images->img_path) !!}" height="350px" width="1150px" alt=" "></a>
                @endforeach
            <div class=" single-grid">
                <h4>{!! $article->title !!}</h4>
                <ul class="blog-ic">
                    <li><a href="#"><span> <i class="glyphicon glyphicon-user"> </i>Super user</span> </a></li>
                    <li><span><i class="glyphicon glyphicon-time"> </i>{{ date('d F, Y', strtotime($article->created_at)) }}</span></li>
                    <li><span><i class="glyphicon glyphicon-eye-open"> </i>Hits:145</span></li>
                </ul>
                <p>{!! $article->html_summary !!}</p>

                <p>{!! $article->html_details !!}</p>
            </div>
            <div class="comments heading">
                <h3>Comments ({{count($article->comments)}})</h3>
                @foreach($article->comments as $comment)
                <div class="media">
                    <div class="media-body">

                        <h4 class="media-heading">{!! $comment->name !!}</h4>
                        <p>{{ date('F d, Y', strtotime($comment->created_at)) }} at {{ date('g:i a', strtotime($comment->created_at)) }}</p><br>

                        <p>{!! $comment->comment !!}</p>
                    </div>


                    <div class="media-right">
                        <a href="#">
                            <img src="{!! asset('images/si.png') !!}" alt=""> </a>
                    </div>

                </div>

                @endforeach

            </div>
            <div class="comment-bottom heading">
                <h3>Leave a Comment</h3>
                {!! Form::open(['route' => 'comment.store', 'files'=> true]) !!}
                {!! Form::text('name', null, ['class' => 'form-control','placeholder'=>'Name']) !!}
                {!! Form::text('email', null, ['class' => 'form-control','placeholder'=>'Email']) !!}
                {!! Form::text('subject', null, ['class' => 'form-control','placeholder'=>'Subject']) !!}
                {!! Form::textarea('comment', null, ['class' => 'form-control','placeholder'=>'Message']) !!}
                {{ Form::hidden('article_id', $article->id) }}
                {!! Form::submit('Send', array('class'=>'btn btn-primary')) !!}


                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<!--end-single-->
@endsection