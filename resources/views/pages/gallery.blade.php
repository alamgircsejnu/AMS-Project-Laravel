@extends('layouts.master')
		@section('content')
	<!--gallery-starts-->
	<div class="gallery">
		<div class="container">
			<div class="gallery-top heading">
				<h3>OUR GALLERY</h3>
			</div>
			<section>
				<ul id="da-thumbs" class="da-thumbs">
					@foreach($images as $image)
					<li>
						<a href="{!! asset($image->img_path) !!}" rel="title" class="b-link-stripe b-animate-go  thickbox">
							<img src="{!! asset($image->img_path) !!}" height="300px" width="400px" alt="" />
							<div>
								<h5>Coffee</h5>
								<span>non suscipit leo fringilla non suscipit leo fringilla molestie</span>
							</div>
						</a>
					</li>
					@endforeach
					<div class="clearfix"> </div>
				</ul>
			</section>
				
		<script type="text/javascript">
			$(function() {
			
				$(' #da-thumbs > li ').each( function() { $(this).hoverdir(); } );

			});
		</script>
        </div>
	</div>
	<!--gallery-end-->
	@endsection