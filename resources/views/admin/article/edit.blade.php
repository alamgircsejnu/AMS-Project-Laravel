{{--{{ dd($editArticle->images) }}--}}

@extends('admin.layouts.default')

@section('css')

        <!-- Timeline CSS -->
<link rel="stylesheet" href="{!! asset('bootstrap/css/bootstrap.min.css') !!}">
<!-- Font Awesome -->
<link rel="stylesheet" href="{!! asset('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css') !!}">
<!-- Ionicons -->
<link rel="stylesheet" href="{!! asset('https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css') !!}">
<!-- Theme style -->
<link rel="stylesheet" href="{!! asset('dist/css/AdminLTE.min.css') !!}">
<!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="{!! asset('dist/css/skins/_all-skins.min.css') !!}">
<!-- iCheck -->
<link rel="stylesheet" href="{!! asset('plugins/iCheck/flat/blue.css') !!}">
<!-- Morris chart -->
<link rel="stylesheet" href="{!! asset('plugins/morris/morris.css') !!}">
<!-- jvectormap -->
<link rel="stylesheet" href="{!! asset('plugins/jvectormap/jquery-jvectormap-1.2.2.css') !!}">
<!-- Date Picker -->
<link rel="stylesheet" href="{!! asset('plugins/datepicker/datepicker3.css') !!}">
<!-- Daterange picker -->
<link rel="stylesheet" href="{!! asset('plugins/daterangepicker/daterangepicker-bs3.css') !!}">
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="{!! asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') !!}">

<link href="{!! asset('dist/css/sb-admin-2.css') !!}" rel="stylesheet">

@endsection

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Add Article</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        @foreach ($errors->all() as $error)
            <p class="alert alert-danger">{{ $error }}</p>
        @endforeach
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif
                    <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                {!! Form::model($editArticle, ['method' => 'PATCH', 'route' => ['admin.article.update', $editArticle->id], 'files' => true]) !!}
                                <div class="form-group">
                                    {!! Form::label('title', 'Title') !!}
                                    {!! Form::text('title', null, ['class' => 'form-control','required','autofocus']) !!}
                                </div>

                                 @foreach($editArticle->images as $images)
                                     <img src="{!! asset($images->img_path) !!}" height="100px" width="100px">
                                    @endforeach
                                        <div class="form-group">
                                    {!! Form::label('img_path', 'Images') !!}
                                    {!! Form::file('img_path') !!}
                                            {!! Form::label('img_path', 'Change Image') !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::label('img_caption', 'Image Caption') !!}
                                    {!! Form::text('img_caption', null, ['class' => 'form-control']) !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::label('html_summary', 'Summery') !!}
                                    {!! Form::textarea('html_summary', null, ['class' => 'form-control']) !!}
                                </div>

                            </div>

                            <div class="col-lg-6">

                                <div class="form-group">
                                    {!! Form::label('sub_title', 'Sub-Title') !!}
                                    {!! Form::text('sub_title', null, ['class' => 'form-control']) !!}
                                </div>

                                {{--.................................................................--}}
                                <div class="form-group {{ $errors->has('category_id') ? 'has-error' : ''}}">
                                    {!! Form::label('articleCategory', 'Category',['class' => 'col-sm-3 control-label']) !!}
                                    <div class="col-sm-9">
                                        @foreach ($categories as $category)
                                            <div class="checkbox">
                                                <label>
                                                    {!! Form::checkbox('category_id[]', $category->id, in_array($category->id, $articleCategories), ['class' => 'field']) !!}{!! $category->title !!}
                                                </label>
                                            </div>
                                        @endforeach

                                        {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                {{--..................................................................--}}


                                <div class="form-group">
                                    {!! Form::label('html_details', 'Details') !!}
                                    {!! Form::textarea('html_details', null, ['class' => 'form-control']) !!}
                                </div>

                                {!! Form::submit('Submit', array('class'=>'btn btn-primary')) !!}
                                {!! Form::reset('Reset', array('class'=>'btn btn-warning')) !!}

                                {!! Form::close() !!}
                            </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
    </div>
@endsection

@section('js')
        <!-- jQuery 2.2.0 -->
    <script src="{!! asset('plugins/jQuery/jQuery-2.2.0.min.js') !!}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{!! asset('https://code.jquery.com/ui/1.11.4/jquery-ui.min.js') !!}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.6 -->
    <script src="{!! asset('bootstrap/js/bootstrap.min.js') !!}"></script>
    <!-- Morris.js charts -->
    <script src="{!! asset('https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js') !!}"></script>
    <script src="{!! asset('plugins/morris/morris.min.js') !!}"></script>
    <!-- Sparkline -->
    <script src="{!! asset('plugins/sparkline/jquery.sparkline.min.js') !!}"></script>
    <!-- jvectormap -->
    <script src="{!! asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') !!}"></script>
    <script src="{!! asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js') !!}"></script>
    <!-- jQuery Knob Chart -->
    <script src="{!! asset('plugins/knob/jquery.knob.js') !!}"></script>
    <!-- daterangepicker -->
    <script src="{!! asset('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js') !!}"></script>
    <script src="{!! asset('plugins/daterangepicker/daterangepicker.js') !!}"></script>
    <!-- datepicker -->
    <script src="{!! asset('plugins/datepicker/bootstrap-datepicker.js') !!}"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="{!! asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') !!}"></script>
    <!-- Slimscroll -->
    <script src="{!! asset('plugins/slimScroll/jquery.slimscroll.min.js') !!}"></script>
    <!-- FastClick -->
    <script src="{!! asset('plugins/fastclick/fastclick.js') !!}"></script>
    <!-- AdminLTE App -->
    <script src="{!! asset('dist/js/app.min.js') !!}"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="{!! asset('dist/js/pages/dashboard.js') !!}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{!! asset('dist/js/demo.js') !!}"></script>

    <script src="//cdn.ckeditor.com/4.5.8/standard/ckeditor.js') !!}"></script>
    <script>
        CKEDITOR.replace('html_details'),{
            uiColor: '#AADC6E'
        };
    </script>
    <script>CKEDITOR.replace('html_summary');</script>
@endsection