<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReplyComment extends Model
{
    protected $fillable = [
        'reply_comment', 'comment_id',
    ];

    public function comments()
    {
        return $this->belongsTo('App\Comment');
    }
}