<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'title', 'left', 'right', 'parent_id',
    ];

    public function articles()
    {
        return $this->belongsToMany('App\Article');
    }
}
