<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Article;
use App\Category;
use App\Image;
use View;
use Auth;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $articles = Article::with('images')->get();
        return view('admin/article/index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::lists('title', 'id');
        return view('admin/article/create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());

        $article = Article::create($request->all());
        $category_ids = $request->input('category_id');
        $article->categories()->attach($category_ids);


        $img = $this->ImageUpload($request->img_path);
        $imgData = new Image();
        $imgData->img_path = $img;
        $imgData->img_caption = $request->img_caption;
        $imgData->extension = $request->img_path->getClientOriginalExtension();
        $imgData->save();
        $lastImageId = $imgData->id;
        $article->images()->attach($lastImageId);

        return redirect('admin/article');

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $article = Article::with('images', 'comments')->find($id);
//        dd($article);
        return View::make('pages.single')->with('article', $article);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        $editArticle = Article::with('images', 'categories')->find($id);
        $articleCategories = $editArticle->categories->lists('id')->toArray();
        $categories=Category::orderBy('created_at', 'desc')->get();
        return view('admin.article.edit', compact('editArticle', 'articleCategories','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (isset($request->img_path) AND !empty($request->img_path)) {
            $updateArticle = Article::find($id);
            //dd($updateArticle);
            $updateArticle->title         = $request->title;
            $updateArticle->sub_title     = $request->sub_title;
            $updateArticle->summary       = $request->summary;
            $updateArticle->details       = $request->details;
            $updateArticle->html_summary  = $request->html_summary;
            $updateArticle->html_details  = $request->html_details;
            $updateArticle->update();

            $categoriesID = $request->category_id;
            if (!empty($categoriesID)) {
                $updateArticle->categories()->sync($categoriesID);
            }

            $menusID = $request->menus_id;
            if (!empty($menusID)) {
                $updateArticle->menus()->sync($menusID);
            }

            foreach ($updateArticle->images as $image) {}
            if (isset($image->img_path) AND ! empty($image->img_path)) {
                unlink($image->img_path);

                $imagePath        = $this->imageUpload($request->img_path);
                $image->img_path  = $imagePath;
                $image->extension = $request->img_path->getClientOriginalExtension();
                $image->update();
            } else {

                $imagePath = $this->imageUpload($request->img_path);

                $article = Article::find($id);
                $articleImage = new Image();
                $articleImage->img_path = $imagePath;
                $articleImage->extension = $request->img_path->getClientOriginalExtension();
                $articleImage->save();

                $imageLastId = $articleImage->id;
                $article->images()->attach($imageLastId);
            }


        } else {

            $updateArticle                = Article::find($id);
            $updateArticle->title         = $request->title;
            $updateArticle->sub_title     = $request->sub_title;
            $updateArticle->summary       = $request->summary;
            $updateArticle->details       = $request->details;
            $updateArticle->html_summary  = $request->html_summary;
            $updateArticle->html_details  = $request->html_details;
            $updateArticle->update();

            $categoriesID = $request->category_id;
            if (!empty($categoriesID)) {
                $updateArticle->categories()->sync($categoriesID);
            }

            $menusID = $request->menus_id;
            if (!empty($menusID)) {
                $updateArticle->menus()->sync($menusID);
            }

        }

        return redirect('admin/article');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteArticle = Article::find($id);

        foreach ($deleteArticle->images as $articleImage) {
            //dd($articleImage->img_path);
        }

        $deleteArticle->delete();

        if (isset($articleImage->img_path) && !empty($articleImage->img_path)) {
            $articleImage->delete();
            unlink($articleImage->img_path);
        }


        return redirect('admin/article');
    }


    public function postIndex()
    {
        $indexArticles = Article::with('images')->orderBy('created_at', 'desc')->paginate(4);

        $sidebarArticles = Article::with('images')->orderBy('created_at', 'desc')->take(3)->get();

//        dd($sidebarArticles);

        $categories = Category::orderBy('created_at', 'desc')->get();
//        dd($indexArticles);
        return view('pages/index', compact('indexArticles', 'sidebarArticles', 'categories'));
    }

    public function ImageUpload($image)
    {
        $extension = $image->getClientOriginalExtension();//get image extension only
        $extensionArray = array("jpg", "jpeg", "png", "gif");

        $path = "image";
        if (in_array($extension, $extensionArray)) {
            $imageOriginalName = $image->getClientOriginalName();//get image full name
            $basename = substr($imageOriginalName, 0, strrpos($imageOriginalName, "."));//get image name without extension
            $imageName = $basename . date("YmdHis") . '.' . $extension;//make new name

            $imageMoved = $image->move($path, $imageName);
            if ($imageMoved) {
                $imagePath = $path . '/' . $imageName;
                return $imagePath;
            }

        } else {
            return "Please insert image";
        }
    }
}
