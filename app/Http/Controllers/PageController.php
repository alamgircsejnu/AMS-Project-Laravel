<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Image;

class PageController extends Controller
{

    public function admin()
    {
        return view('admin.index');
    }

    public function about()
    {
        return view('pages.about');
    }

    public function contact()
    {
        return view('pages.contact');
    }

    public function gallery()
    {
        $images=Image::with('articles')->get();
        return view('pages.gallery',compact('images'));
    }

    public function typo()
    {
        return view('pages.typo');
    }


    public function single()
    {
        return view('pages.single');
    }


}
