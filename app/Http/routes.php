<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', function () {
//    return view('pages.index');
//});

Route::auth();

//..........................route group....................

Route::group(['middleware' => 'auth'], function () {

    Route::get('/admin', 'PageController@admin');
    Route::resource('/admin/article', 'ArticleController');
    Route::resource('/admin/category', 'CategoryController');
    Route::resource('/admin/image', 'ImageController');
    Route::resource('/admin/menu', 'MenuController');
});


Route::get('/', 'ArticleController@postIndex');
Route::get('/category/{id}', 'CategoryController@categoryWiseArticles');
Route::get('/home', 'HomeController@index');
Route::resource('/comment', 'CommentController');
Route::resource('/reply-comment', 'ReplyCommentController');

//................... page controller...............

Route::get('/about', 'PageController@about');
Route::get('/contact', 'PageController@contact');
Route::get('/gallery', 'PageController@gallery');
Route::get('/typo', 'PageController@typo');
Route::get('pages/single', 'PageController@single');







