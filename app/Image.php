<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = [
        'sub_title', 'extention', 'size',
    ];

    public function articles()
    {
        return $this->belongsToMany('App\Article');
    }
}
