<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
     protected $fillable = [
        'title', 'sub_title', 'summary', 'html_summary', 'details', 'html_details',
    ];
    protected $dates=['deleted_at'];

    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }

    public function images()
    {
        return $this->belongsToMany('App\Image');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }
    public function reply_comments()
    {
        return $this->hasManyThrough('App\ReplyComments', 'App\Comment');
    }
}
