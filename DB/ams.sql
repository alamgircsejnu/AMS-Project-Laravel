-- phpMyAdmin SQL Dump
-- version 4.6.0
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jun 09, 2016 at 02:59 AM
-- Server version: 10.1.9-MariaDB-log
-- PHP Version: 5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ams`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sub_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `summary` text COLLATE utf8_unicode_ci,
  `html_summary` text COLLATE utf8_unicode_ci,
  `details` text COLLATE utf8_unicode_ci,
  `html_details` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `title`, `sub_title`, `summary`, `html_summary`, `details`, `html_details`, `created_at`, `updated_at`, `deleted_at`) VALUES
(11, 'Lorem ipsum: usage', '', NULL, 'Lorem ipsum is a pseudo-Latin text used in web design, typography, layout, and printing in place of English to emphasise design elements over content. It\'s also called placeholder (or filler) text. ', NULL, 'It\'s a convenient tool for mock-ups. It helps to outline the visual elements of a document or presentation, eg typography, font, or layout. Lorem ipsum is mostly a part of a Latin text by the classical author and philosopher Cicero. Its words and letters have been changed by addition or removal, so to deliberately render its content nonsensical; it\'s not genuine, correct, or comprehensible Latin anymore. While lorem ipsum\'s still resembles classical Latin, it actually has no meaning whatsoever. As Cicero\'s text doesn\'t contain the letters K, W, or Z, alien to latin, these, and others are often inserted randomly to mimic the typographic appearence of European languages, as are digraphs not to be found in the original', '2016-06-08 22:55:19', '2016-06-08 22:55:19', NULL),
(12, 'Lorem Ipsum Generator', '', NULL, 'Lorem ipsum dolor sit amet, nulla ut nulla, erat nec faucibus mollis sit. In sed mauris vestibulum amet placerat sit. Rhoncus mi est, a urna, tincidunt massa vitae montes, nunc id. Id elit iaculis, in arcu pulvinar nunc porttitor eget ultricies, eu euismod tortor neque sed lorem, enim proin integer gravida netus et. Dapibus dolore, eu sed mi, molestie vulputate ipsum posuere vel turpis mauris,', NULL, ' imperdiet commodo wisi, quisque ipsum nec mauris. Ante quis senectus aliquam egestas. Nonummy eget morbi dui nunc, mattis et in justo, nibh sit pretium, tellus elit a ut rhoncus, consequat ac placerat posuere adipiscing varius. Ac libero, dictumst condimentum libero at euismod, pharetra nibh, massa vitae eget eget quam, ipsum magna nonummy libero in rhoncus feugiat. Adipiscing luctus scelerisque, scelerisque eu eu arcu, ante lacus sit. A sollicitudin, ultricies tortor feugiat ut, ipsum in amet. Semper vehicula, malesuada vel. Proin tempor blandit non mi leo, ac ligula erat suspendisse mi minima elit, nihil justo, pellentesque massa blandit, praesent eget suscipit commodo.\r\nMi sem lorem pellentesque sed pellentesque, eget elementum eget nunc sapien enim, nulla dui ac est. Commodo auctor volutpat mi magna, metus pulvinar orci vel, quam in, vestibulum porta ligula. Sem nibh integer dui gravida sit, ligula nisi nunc auctor eu, eu malesuada id nec, mauris duis ultricies aenean. Quam fusce quis id lacinia in, justo magna vestibulum, tincidunt lacus pulvinar lectus pede rutrum. At congue id in ac sapien, et tempus lorem id lectus, feugiat vivamus eu volutpat duis, neque nec phasellus cupidatat malesuada malesuada lorem. Vivamus orci diam hymenaeos sem, parturient nonummy adipiscing at nec, nam porta. Mattis parturient magna rutrum wisi magna. Mi urna dignissim mauris.\r\nArcu etiam mauris auctor faucibus pellentesque nam, non eu nunc, curabitur eros vel eu, ut ut dolor integer, aptent amet bibendum eu. Quis nam. A sed, at ridiculus elit, rhoncus rutrum per cubilia ut venenatis quis, elit viverra turpis id suspendisse semper arcu. Integer pretium sit, et nec adipisicing volutpat nulla, sed nulla magna donec congue, nibh iaculis quis malesuada, sollicitudin velit enim in morbi velit amet. Lacus purus volutpat, elit nonummy eu nunc nisl, non massa dui ac, libero dolor sit augue ut sed lorem. Sit aenean tempor nulla.\r\nPede et, nullam dui morbi, ornare orci ipsum ultricies libero. Dapibus est id eu, sociosqu praesent massa rutrum, volutpat sapien, rhoncus velit. Purus at donec augue donec semper non, adipiscing sit aptent vel aliquam. Dictumst justo ante pede, diam nam, vehicula odio est posuere orci aptent, nec ac sollicitudin nullam eu sed velit. Dignissim elit, dapibus iaculis.\r\n+\r\n', '2016-06-08 22:56:10', '2016-06-08 22:56:10', NULL),
(13, 'Lorem Ipsum', '', NULL, 'Lorem ipsum dolor sit amet, wisi sed risus velit, massa nunc, aliquam scelerisque nulla in molestie aliquam. Et mauris sapien libero, donec excepteur laoreet tincidunt netus, mus proin rutrum nullam phasellus neque, magna elit nibh dui metus. Mi magnam integer venenatis pulvinar. Habitant laoreet vel sit imperdiet vehicula pretium, mauris adipiscing vel et deserunt aliquam, fringilla curabitur adipiscing, duis sit varius. ', NULL, 'Tincidunt metus volutpat enim mauris, nam ante, in cras arcu ipsum et feugiat pellentesque, vel et lacinia, quis est placerat quis. Ipsum ut amet dolor eget sit. Quis lorem, eros facilis urna mollis praesent elit montes. Fringilla enim nostrum, vitae natoque elementum, nam lacus augue ad suspendisse massa massa.\r\nOdio et. Venenatis vestibulum accumsan ante porta a posuere, neque torquent quis augue habitasse aut ornare, non luctus, eu ut sed maecenas dui felis lectus. Lobortis eu nulla porttitor cras, id eu tristique ultricies pharetra, id rhoncus praesent consectetuer scelerisque aliquam, sed potenti morbi nunc lobortis ante. Fringilla adipiscing pellentesque urna tincidunt nulla at. In nullam ac magna nec, non proin aliquam vivamus vel. Etiam lectus nam, leo quam dui ultricies quis aliquam, adipiscing per, neque netus. Feugiat pulvinar, et cursus. Mollis curabitur eu lobortis, tellus sit accumsan cumque, elit faucibus enim, a rhoncus. Neque ante ante vestibulum proin orci, nam et leo, at ac ac urna quam, nisl fringilla pede tincidunt urna dui.\r\nCum ut ante erat suspendisse, ut vivamus arcu praesent sit, arcu erat vitae ratione eu, tellus dolorum pellentesque eleifend. Mauris ac sed ut non natoque, platea lacinia massa ipsum sed leo neque, sollicitudin id. Mauris a proin mauris, non vivamus sapien ac nulla, pellentesque sodales pellentesque. Nec parturient ullamcorper adipiscing porttitor in, tortor volutpat auctor, magna eros egestas. Purus modi nec, magna laoreet lectus mauris. Arcu nibh sem sapien. Hymenaeos sed sed consectetuer, orci malesuada arcu nec, elit lacus felis in nisl vel enim.\r\nSem faucibus nec eu egestas integer, nulla condimentum iaculis libero morbi nec, vestibulum iaculis minima purus. Tristique arcu quam suspendisse, ipsum suspendisse ut eget. Lacinia sit commodo et pariatur mattis porta, laoreet vitae ac justo dis, a in dictum sit nec, eros dolor faucibus odio, amet egestas. Nascetur quisque sociosqu nisl quis. Montes interdum iaculis at. Sit vivamus ultricies mattis faucibus, diam bibendum auctor ultricies ligula, diam aenean pede sollicitudin ea nonummy enim. Proin eu nostrud sem, urna fringilla consectetuer magna mattis neque. Nec donec, viverra eu.\r\n+', '2016-06-08 22:57:20', '2016-06-08 22:57:20', NULL),
(14, 'Try the new improved Lorem Ipsum Generator', '', NULL, 'Lorem ipsum dolor sit amet, ullamcorper at erat at sed, eros nibh elit per, luctus orci nonummy sapien mauris sapien ridiculus. Sit a adipiscing. Eleifend ut non dolor, nec iaculis ut ac, pellentesque vivamus curabitur orci vestibulum ante erat, porttitor tristique id tincidunt aenean ante odio. Turpis parturient et dapibus justo, sagittis aenean dis, felis omnis phasellus orci potenti urna fermentum, quisque magna ut rhoncus consequat nec officia. ', NULL, 'Volutpat egestas euismod senectus tellus tortor eget, suspendisse massa ac et, in nibh semper pellentesque potenti sollicitudin, luctus cum torquent. Id vitae scelerisque.\r\nVitae a nec ut luctus varius donec, et enim orci, faucibus fusce mi vel, elit ullamcorper quisque ipsum duis, etiam ultricies magnis lectus mi lobortis. Et odio, commodo qui amet tristique convallis, fusce in facilisis quam, orci tincidunt, praesent sem mi ante eu proin. Ipsum est dapibus. Ipsum dictum nonummy amet phasellus, lacus nullam imperdiet ac suspendisse amet, urna per, volutpat sollicitudin quis. Ut lorem vitae, mauris hendrerit sodales turpis, pede rutrum sagittis magna sodales vulputate, mus amet in euismod, felis lacus mattis. Ultricies elit nulla maecenas quis.\r\nAut suscipit velit, justo morbi vitae egestas. Amet a enim augue erat volutpat, ante iaculis duis, urna est morbi vel ante dolor, taciti mattis ante nonummy sollicitudin, vestibulum tincidunt malesuada eget. Nunc inceptos tellus etiam dolor dapibus in, inceptos vitae wisi vel dolor, faucibus id posuere. Est eros, sapien ante parturient. Velit pede, non lacus duis varius ac enim, magna odio dolor nonummy metus a dui, sollicitudin in ante mattis. Metus sit accumsan diam lacinia mollis, arcu ornare, torquent massa litora.\r\nDuis quis velit tempor, vehicula enim vel, rutrum nec wisi, in nullam lectus nunc, ligula lorem faucibus congue odio. Rhoncus pellentesque eget pede. Ut adipiscing vel in consequat, malesuada ultricies et aliquam eget. Amet nunc elementum sed quam justo elit. Est dui nec tristique nulla nec, nullam eget felis per, ultricies tellus suspendisse quam vitae adipiscing suspendisse.', '2016-06-08 22:58:22', '2016-06-08 22:58:22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `article_category`
--

CREATE TABLE `article_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `article_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `article_category`
--

INSERT INTO `article_category` (`id`, `article_id`, `category_id`) VALUES
(13, 11, 7),
(14, 12, 7),
(15, 12, 8),
(16, 13, 8),
(17, 14, 9),
(18, 12, 10),
(19, 14, 10);

-- --------------------------------------------------------

--
-- Table structure for table `article_image`
--

CREATE TABLE `article_image` (
  `id` int(10) UNSIGNED NOT NULL,
  `article_id` int(10) UNSIGNED NOT NULL,
  `image_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `article_image`
--

INSERT INTO `article_image` (`id`, `article_id`, `image_id`) VALUES
(4, 11, 4),
(5, 12, 5),
(6, 13, 6),
(7, 14, 7);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `left` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `right` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `left`, `right`, `parent_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(7, 'ARTS & ENTERTAINMENT', '', '', 0, '2016-06-09 02:53:04', '2016-06-09 02:53:04', NULL),
(8, 'SPORTS AND FITNESS', '', '', 0, '2016-06-09 02:53:25', '2016-06-09 02:53:25', NULL),
(9, 'INFORMATION TECHNOLOGY', '', '', 0, '2016-06-09 02:53:44', '2016-06-09 02:53:44', NULL),
(10, 'COMPUTERS', '', '', 0, '2016-06-09 02:54:07', '2016-06-09 02:54:07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `article_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(10) UNSIGNED NOT NULL,
  `img_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img_caption` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `extension` char(4) COLLATE utf8_unicode_ci NOT NULL,
  `size` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `img_path`, `img_caption`, `extension`, `size`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'image/download20160608085635.jpg', 'Beautiful Car', 'jpg', '', '2016-06-08 15:56:35', '2016-06-08 15:56:35', NULL),
(2, 'image/images (1)20160608085943.jpg', 'sffgbv', 'jpg', '', '2016-06-08 15:59:43', '2016-06-08 15:59:43', NULL),
(3, 'image/tom-and-jerry-desktop-wallpaper-51379-53077-hd-wallpapers20160609045207.jpg', 'ewfgfhdf', 'jpg', '', '2016-06-08 22:52:07', '2016-06-08 22:52:07', NULL),
(4, 'image/images20160609045519.jpg', 'Beautiful Car', 'jpg', '', '2016-06-08 22:55:19', '2016-06-08 22:55:19', NULL),
(5, 'image/download20160609045610.jpg', 'Beautiful Car', 'jpg', '', '2016-06-08 22:56:10', '2016-06-08 22:56:10', NULL),
(6, 'image/images (1)20160609045721.jpg', 'Beautiful Car', 'jpg', '', '2016-06-08 22:57:21', '2016-06-08 22:57:21', NULL),
(7, 'image/download (1)20160609045822.jpg', 'Beautiful Car', 'jpg', '', '2016-06-08 22:58:22', '2016-06-08 22:58:22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `left` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `right` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_06_04_103429_create_articles_table', 1),
('2016_06_06_042341_create_categories_table', 1),
('2016_06_06_054606_create_images_table', 1),
('2016_06_06_065927_create_menus_table', 1),
('2016_06_06_105854_create_article_category_pivot_table', 1),
('2016_06_06_145701_create_article_image_pivot_table', 1),
('2016_06_08_075030_create_comments_table', 1),
('2016_06_08_075302_create_reply_comments_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reply_comments`
--

CREATE TABLE `reply_comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `reply_comment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comment_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Alamgir Hossain', 'alamgircsejnu@gmail.com', '$2y$10$6SUEL..wUpN6/8jIrCuhXexdFj/tCk7DzsflYrvmp4P.750SBYPk6', 'r6rh9q1tTZdLmrw4L5tQQUBoGnBkwkmgZYqjFwLDwDf2NgGmN9obU8fg0sR1', '2016-06-08 23:03:04', '2016-06-08 23:04:26');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `article_category`
--
ALTER TABLE `article_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `article_category_article_id_index` (`article_id`),
  ADD KEY `article_category_category_id_index` (`category_id`);

--
-- Indexes for table `article_image`
--
ALTER TABLE `article_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `article_image_article_id_index` (`article_id`),
  ADD KEY `article_image_image_id_index` (`image_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_article_id_index` (`article_id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `reply_comments`
--
ALTER TABLE `reply_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reply_comments_comment_id_index` (`comment_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `article_category`
--
ALTER TABLE `article_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `article_image`
--
ALTER TABLE `article_image`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `reply_comments`
--
ALTER TABLE `reply_comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `article_category`
--
ALTER TABLE `article_category`
  ADD CONSTRAINT `article_category_article_id_foreign` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `article_category_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `article_image`
--
ALTER TABLE `article_image`
  ADD CONSTRAINT `article_image_article_id_foreign` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `article_image_image_id_foreign` FOREIGN KEY (`image_id`) REFERENCES `images` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_article_id_foreign` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `reply_comments`
--
ALTER TABLE `reply_comments`
  ADD CONSTRAINT `reply_comments_comment_id_foreign` FOREIGN KEY (`comment_id`) REFERENCES `comments` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
